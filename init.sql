--CREATE TABLE IF NOT EXISTS subjects(
--    id INTEGER PRIMARY KEY,
--    name TEXT);
--
--CREATE TABLE IF NOT EXISTS students(
--    id INTEGER PRIMARY KEY,
--    name TEXT,
--    mark INTEGER REFERENCES mark(id));
--
--CREATE TABLE IF NOT EXISTS mark(
--    id INTEGER PRIMARY KEY,
--    student_id INTEGER REFERENCES students(id),
--    subject_id INTEGER REFERENCES subjects(id),
--    grade INTEGER NOT NULL);
--
--INSERT INTO subjects VALUES (1, 'Algebra');
--INSERT INTO subjects VALUES (2, 'Informatika');
--INSERT INTO subjects VALUES (3, 'Fizika');
--
--INSERT INTO mark VALUES (1, 1, 1, 100);
--INSERT INTO mark VALUES (2, 2, 1, 50);
--INSERT INTO mark VALUES (3, 3, 2, 99);
--INSERT INTO mark VALUES (4, 4, 1, 30);
--
--insert into students VALUES (1, 'Denigger Artem', 1);
--insert into students VALUES (2, 'Forutemu Petrovich', 2);
--insert into students values (3, 'Hideo Kojima', 3);
--insert into students values (4, 'Neblinova Sveta', 4);

-- Создание таблицы subjects
CREATE TABLE subjects (
  subject_id SERIAL PRIMARY KEY,
  subject VARCHAR(255) NOT NULL
);

-- Вставка данных в таблицу subjects
INSERT INTO subjects (subject_id, subject) VALUES (1, 'Algebra');
INSERT INTO subjects (subject_id, subject) VALUES (2, 'Informatika');
INSERT INTO subjects (subject_id, subject) VALUES (3, 'Fizika');

-- Создание таблицы students
CREATE TABLE students (
  student_id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);

-- Вставка данных в таблицу students
INSERT INTO students (student_id, name) VALUES (1, 'Denigger Artem');
INSERT INTO students (student_id, name) VALUES (2, 'Forutemu Petrovich');
INSERT INTO students (student_id, name) VALUES (3, 'Hideo Kojima');
INSERT INTO students (student_id, name) VALUES (4, 'Neblinova Sveta');

-- Создание таблицы marks
CREATE TABLE marks (
  mark_id SERIAL PRIMARY KEY,
  student_id INT NOT NULL,
  subject_id INT NOT NULL,
  grade INT NOT NULL,
  CONSTRAINT fk_student
    FOREIGN KEY (student_id)
    REFERENCES students (student_id),
  CONSTRAINT fk_subject
    FOREIGN KEY (subject_id)
    REFERENCES subjects (subject_id)
);

-- Вставка данных в таблицу marks
INSERT INTO marks (mark_id, student_id, subject_id, grade) VALUES (1, 1, 1, 100);
INSERT INTO marks (mark_id, student_id, subject_id, grade) VALUES (2, 2, 1, 50);
INSERT INTO marks (mark_id, student_id, subject_id, grade) VALUES (3, 3, 2, 99);
INSERT INTO marks (mark_id, student_id, subject_id, grade) VALUES (4, 4, 1, 30);