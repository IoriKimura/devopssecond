import psycopg2


# Вывести ФИО, предмет и оценки студента с наилучшей успеваемостью
def main():
    conn = psycopg2.connect(
        host="DB",
        dbname="DenegaBD",
        user="root",
        password="root",
        port="5432"
    )
    with conn:
        with conn.cursor() as cursor:
            query = """SELECT
                  students.name AS ФИО,
                  subjects.subject AS Предмет,
                  marks.grade AS Оценка
                FROM
                  students
                  JOIN marks ON students.student_id = marks.student_id
                  JOIN subjects ON marks.subject_id = subjects.subject_id
                WHERE
                  (marks.subject_id, marks.grade) IN (
                    SELECT
                      subject_id,
                      MAX(grade)
                    FROM
                      marks
                    GROUP BY
                      subject_id
                  );"""
            cursor.execute(query)
            response = cursor.fetchall()
            with open('result/best_student.txt', 'w') as f:
                f.write(str(response))
                f.close()


if __name__ == '__main__':
    main()
