FROM python:3.9-alpine
RUN pip install psycopg2-binary
COPY main.py /main.py
CMD ["python","main.py"]
